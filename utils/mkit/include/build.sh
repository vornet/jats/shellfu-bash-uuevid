#!/bin/bash
# MKit - simple install helper
# See LICENSE file for copyright and license details.

mkit_import ini
mkit_import facts


__build1() {
    #
    # Process one skeleton $1 of type $3 (or guessed) to path $2
    #
    local srcpath=$1    # skeleton path
    local dstpath=$2    # destination meaty animal path
    local ftype=$3      # file/builder type
    test -n "$dstpath"  || dstpath=${srcpath%.skel}
    test -n "$ftype"    || ftype=MKIT_COMMON
    debug_var srcpath dstpath ftype
    <"$srcpath" __build1_ftype "$ftype" >"$dstpath"
    __rec_built "$dstpath"
}

__build1_ftype() {
    #
    # Build a file of type $1; fom stdin to stdout
    #
    local ftype=$1      # file/builder type
    case $ftype in
        MKIT_COMMON)    __expand_macros "build:macros" ;;
        rpmstuff)       __expand_macros "rpmstuff:macros" ;;
        debstuff)       __expand_macros "debstuff:macros" ;;
        *)              die "unknown file type: $ftype" ;;
    esac
}

__macrol() {
    #
    # List known macros
    #
    find "${MacroDirs[@]}" -mindepth 1 -depth -printf '%P\n' \
      | sed 's/[.]/:/g'
}

__macror() {
    #
    # Read macro $1
    #
    local key=$1
    find "${MacroDirs[@]}" -mindepth 1 -depth -name "$key" \
      | head -1 \
      | xargs -r cat
}

__macrow() {
    #
    # Write stdin as macro $1
    #
    local fqkey=$1
    local file="$MKIT_LOCAL/data/${fqkey//:/.}"
    mkdir -p "${file%/*}"
    cat >"$file"
}

__expand_line() {
    #
    # Expand macro from config in single line $1
    #
    # If macro value has multiple lines, repeat original line with
    # different substitution.
    #
    # E.g. if macro value is "foo\nbar" and macro name is __FOO__,
    # line `see: "__FOO__"` will expand to two lines: `see: "foo"`
    # and `see: "bar"`.
    #
    local line=$1   # line to process
    local mname     # macro name
    local mvline    # line of macro value
    local xline     # expanded line
    xline=$line
    for mname in $(__macrol); do
        if ! test "${line//$mname}" == "$line"; then
            xline=$(
                while IFS= read -r mvline; do
                    echo "${line//$mname/$mvline}"
                done <<<"$(__macror "$mname")"
            )
        fi
        line=$xline
    done
    echo "$xline"
    return 1
}

__expand_macros() {
    #
    # Read stdin, expanding macros from extra sections $@
    #
    local MacroDirs=()  # directories to lookup macros in
    local mdir          # every ^^
    local line          # each line on stdin
    local bltndata="$MKIT_LOCAL/data/MKIT_BUILTIN"
    local usrdata="$MKIT_LOCAL/data/macros"
    test -d "$bltndata" && MacroDirs+=("$bltndata")
    test -d "$usrdata" && MacroDirs+=("$usrdata")
    for extra in "$@"; do
        mdir="$MKIT_LOCAL/data/${extra//:/.}"
        test -d "$mdir" || continue
        MacroDirs+=("$mdir")
    done
    while IFS= read -r line; do
        __expand_line "$line"
    done
}

__qfs() {
    #
    # Quote for our sed scipt's RHS
    #
    sed '
        s:\\:\\\\:g
        s:|:\\|:g
    '
}

__cached() {
    #
    # Cached value $1 of function $1()
    #
    # In order to support git-less builds, some values might be cached
    # in $MKIT_LOCAL.  This function gets file $1 from that cache (cache
    # hit) or re-creates it (cache miss), but prints its body in either
    # case.
    #
    # The command to re-create file is the same as the key (ie. no
    # arguments).
    #
    local name=$1
    __local_get "$name" && return 0
    "$name" | __local_putb "$name"
    __local_get "$name"
}

__local_putb() {
    #
    # Make file $1 in $MKIT_LOCAL from stdin and mark as built
    #
    local fpath=$1
    __local_put "$fpath" && __rec_built "$MKIT_LOCAL/$fpath"
}

__local_put() {
    #
    # Make file $1 in $MKIT_LOCAL from stdin
    #
    local fpath="$MKIT_LOCAL/$1"
    { mkdir -p "${fpath%/*}" && cat >"$fpath"; } \
     || die "cannot write to local cache: $fpath"
}

__local_get() {
    #
    # Read file $1 in $MKIT_LOCAL
    #
    local fpath="$MKIT_LOCAL/$1"
    cat "$fpath" 2>/dev/null
}

__rec_built() {
    #
    # Record file $1 for deletion on `clean`
    #
    local file=$1
    mkdir -p "$MKIT_LOCAL"
    echo "1:$file" >> "$MKIT_LOCAL/built.lst"
}

__rec_builtr() {
    #
    # Record dir $1 for recursive  deletion on `clean`
    #
    local path=$1
    mkdir -p "$MKIT_LOCAL"
    echo "r:$path" >> "$MKIT_LOCAL/built.lst"
}

_mkit_builddata() {
    #
    # Build config data
    #
    local macro
    local section
    test -d "$MKIT_LOCAL/data/build.macros" && {
        warn "mkit: using cached _mkit_builddata: $(date -Isec -r "$MKIT_LOCAL/data/build.macros")"
        warn "mkit:   (hint: run 'make clean' to regenerate it)"
        return 0
    }
    __rec_builtr "$MKIT_LOCAL/data"
    for macro in $(ini lskeys "build:macros"); do
        ini values "build:macros:$macro" | __macrow "build:macros/$macro"
    done
}

_mkit_metadata() {
    #
    # Build meta data
    #
    test -d "$MKIT_LOCAL/data/MKIT_BUILTIN" && {
        warn "mkit: using cached _mkit_metadata: $(date -Isec -r "$MKIT_LOCAL/data/MKIT_BUILTIN")"
        warn "mkit:   (hint: run 'make clean' to regenerate it)"
        return 0
    }
    __rec_builtr "$MKIT_LOCAL/data"
    echo "$MKIT_VERSION"            | __macrow MKIT_BUILTIN/__MKIT_MKIT_VERSION__
    ini 1value project:name         | __macrow MKIT_BUILTIN/__MKIT_PROJ_NAME__
    ini 1value project:codename     | __macrow MKIT_BUILTIN/__MKIT_PROJ_CODENAME__
    ini 1value project:license      | __macrow MKIT_BUILTIN/__MKIT_PROJ_LICENSE__
    ini 1value project:pkgname      | __macrow MKIT_BUILTIN/__MKIT_PROJ_PKGNAME__
    ini 1value project:tagline      | __macrow MKIT_BUILTIN/__MKIT_PROJ_TAGLINE__
    ini 1value project:maintainer   | __macrow MKIT_BUILTIN/__MKIT_PROJ_MAINTAINER__
    ini 1value project:vcs_browser  | __macrow MKIT_BUILTIN/__MKIT_PROJ_VCS_BROWSER__
    __cached git_lasthash           | __macrow MKIT_BUILTIN/__MKIT_PROJ_GIT_LASTHASH__
    __cached git_lastsummary        | __macrow MKIT_BUILTIN/__MKIT_PROJ_GIT_LASTSUMMARY__
    __cached semver                 | __macrow MKIT_BUILTIN/__MKIT_PROJ_VERSION__
    for section in macros debstuff:macros rpmstuff:macros; do
        for macro in $(ini lskeys "$section"); do
            ini values "$section:$macro" | __macrow "$section/$macro"
        done
    done
}

__subdirs() {
    #
    # List direct sub-directories of $1
    #
    find "$1" -maxdepth 1 -mindepth 1 -printf '%P\n' -type d
}

__subfiles() {
    #
    # List direct sub-files of $1
    #
    find "$1" -maxdepth 1 -mindepth 1 -printf '%P\n' -type f
}

_mkit_data() {
    #
    # Build sampler showing all macro values
    #
    warn "DEPRECATION: _mkit_data is deprecated and will be removed soon!"
    warn "DEPRECATION: Use _mkit_show_metadata or _mkit_show_build instead."
    warn "DEPRECATION: (redirecting to _mkit_show_metadata)"
    _mkit_show_metadata
}

_mkit_show_metadata() {
    #
    # Show sampler of macro values
    #
    __show_msection MKIT_BUILTIN
    __show_msection macros
    __show_msection rpmstuff:macros
    __show_msection debstuff:macros
}

_mkit_show_builddata() {
    #
    # Show sampler of config values
    #
    __show_msection build:macros
}

__show_msection() {
    #
    # Show macros of section $1
    #
    local section=$1
    local mname
    local first=true
    local label=$section
    local secdir="$MKIT_LOCAL/data/${section//:/.}"
    test -d "$secdir" || return 0
    test "$section" == MKIT_BUILTIN && label="(builtin)"
    for mname in $(__subfiles "$secdir"); do
        $first && echo "$label:"; first=false
        echo "    $mname => '$(<"$secdir/$mname")'"
    done
}

build() {
    #
    # Add meat to all skeletons
    #
    local srcpath   # each source path
    find . -type f -name '*.skel' \
     | while read -r srcpath; do
           __build1 "$srcpath"
       done
}

clean() {
    #
    # Clean up tree after building
    #
    local path
    local line
    local depth
    test -f "$MKIT_LOCAL/built.lst" || return 0
    {
        cat "$MKIT_LOCAL/built.lst"
        echo "1:$MKIT_LOCAL/built.lst"
        echo "1:$MKIT_LOCAL"
    } \
      | grep -v -e '\.\.' -e ^/ -e '^~' \
      | while IFS=: read -r depth path; do
            test -e "$path" || continue
            case $depth in
                1)  warn "removing: $path"
                    test -d "$path" \
                     && rmdir -p --ignore-fail-on-non-empty "$path"
                    test -f "$path" && rm "$path"
                    ;;
                r)  warn "removing recursively: $path"
                    rm -r "$path"
                    ;;
                *)  warn "invalid built.lst format!"
                    ;;
            esac
        done
    true
}

debstuff() {
    #
    # Build Debian stuff (eamed tarball, debian dir)
    #
    local version       # package version
    local debian_skel   # 'debian' folder skeleton
    local dfsrc         # each source file from ^^
    local dftgt         # each built packaging file
    version=$(__cached semver)

    # tarball - we should already have by means of 'dist'
    #
    mv "${MKIT_PROJ_PKGNAME}-$version.tar.gz" \
       "${MKIT_PROJ_PKGNAME}_$version.orig.tar.gz" \
     || die "could not rename tarball"
    __rec_built "${MKIT_PROJ_PKGNAME}_$version.orig.tar.gz"

    # read content of each mandatory file from debian_skel
    #
    debian_skel=$(ini 1value dist:debstuff)
    test -n "$debian_skel" || die "dist:debstuff not specified"
    test -d "$debian_skel" || die "debian directory template found: $debian_skel"
    mkdir -p debian/source
    find "$debian_skel" -type f \
      | while read -r dfsrc; do
            dftgt="debian/${dfsrc#$debian_skel}"
            mkdir -p "$(dirname "$dftgt")"
            __build1 "$dfsrc" "$dftgt" debstuff
        done
    __rec_built debian
}

dist() {
    #
    # Create distributable tarball
    #
    #FIXME: lacking Makefile skills, we do this step twice for
    #       rpmstuff, hence -f hack for gzip
    #
    local version           # tarball version
    local git_lasthash      # last git commit hash
    local dirname           # directory and tarball name
    version=$(semver)
    dirname=$MKIT_PROJ_PKGNAME-$version
    git_lasthash=$(git_lasthash)
    mkdir -p "$dirname"
    ini values "dist:tarball" | xargs -I DIST_ITEM cp -R DIST_ITEM "$dirname"
    mkdir -p "$dirname/.mkit"
    echo -n "$version" > "$dirname/.mkit/semver"
    echo -n "$git_lasthash" > "$dirname/.mkit/git_lasthash"
    cp -r "$MKIT_LOCAL/data" "$dirname/.mkit/"
    tar -cf "$dirname.tar" "$dirname"
    gzip -f "$dirname.tar"      # see above FIXME
    __rec_built "$dirname.tar.gz"
    rm -rf "$dirname"
}

rpmstuff() {
    #
    # Build specfile
    #
    local specname=$MKIT_PROJ_PKGNAME.spec      # .spec filename
    local specsrc                               # source of specfile
    specsrc="$(ini 1value "dist:rpmstuff")"
    test -n "$specsrc" || die "dist:rpmstuff not specified"
    test -f "$specsrc" || die "specfile template not found: $specsrc"
    __build1 "$specsrc" "$specname" rpmstuff
}
